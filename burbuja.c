#include <stdio.h>
#define Tipo int
void init_array(Tipo array[],int tam){
  int i=0;
  for(;i<tam;i++)
    array[i]=rand()%tam;
}
void print_array(Tipo array[],int tam){
  int i=0;
  for(;i<tam;i++)
    printf("%d ",array[i]);
  printf("\n");   
}
/*
 * Algoritmo de burbuja mejorado 
 * @miguelCabrera 3-Dic-2015 
 * Toma un arreglo de elementos y el tamaño del mismo 
*/
void burbuja_sort(Tipo a[],int tam){
  int i=0,j=0;
  Tipo temp; 
  for(j=0;j<tam;j++){ // algoritmo del tipo O(n^2)   
    for(i=0;i<tam-1;i++){
      if(a[i]>=a[i+1]){
	temp=a[i+1];
	a[i+1]=a[i];
	a[i]=temp;
	//	printf("%d\n ",a[i]);  
      }// fin del if 
    }//fin de for interno
  }// fin del primer for
}
void swap(int *a,int *b){
  int t=&a;
  printf("%d",t);
  //a=&b;
  //b=&t;
}
void burbuja_sort_2(Tipo a[], int tam){
  int i=0,j=0;
  Tipo temp; 
  for(j=0;j<tam;j++){ // algoritmo del tipo O(n^2)   
    for(i=1;i<tam-1;i++){
      if(a[j]<a[i]){
	swap(&a[j],&a[i]);
	/*	temp=a[i];
	a[i]=a[j];
	a[j]=temp;*/
//	printf("%d\n ",a[i]);  
      }// fin del if 
    }//fin de for interno
  }// fin del primer for
}
int main(int argc,char *argv[]){
  Tipo arr[10];
  init_array(arr,10);
  print_array(arr,10);
  burbuja_sort_2(arr,10);
  print_array(arr,10);
  return 0;
}

