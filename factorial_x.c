#include <stdio.h>
int facto(int x){
  if(x==0)return 1;
  return x*facto(x-1);
}
int facto_lineal(int x){
  int i,f=1;
  for(i=1;i<=x;i++){
    f*=i;
  }
  return f; 
}
int main(void){
  printf("%d",facto_lineal(15));
  return 0;
}
