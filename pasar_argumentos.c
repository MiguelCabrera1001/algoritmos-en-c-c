#include <stdio.h>
/**
 * @miguelCabrera 
 * Programa que lee desde linea de comandos los argumentos del programa 
 * Depués llama a la funcion MAX(a,b) que devuelve el maximo de dos numeros
 * 09/12/2015
 */
int MAX(int a,int b ){
   return (a>b)? a:b; 
}
int main(int argc,char *argv[]){
  if(argc!=3) { printf("Debes incluir 3 argumentos\n"); return -1;}
  int t=1;
  int a=atoi(argv[1]),b=atoi(argv[2]);
  while(t<argc){
    printf("%s\n",argv[t]);
    t++;
   }
  printf("\n%d\n",MAX(a,b));
  return 0;
}
