#include <stdio.h>
#include <malloc.h>
/*
 *  @miguelCabrera
 *  Programa que implementa la estructura basica 
 *  de una lista ligada y una la funcion que recorre la lista 
 *  09/12/2015
*/
typedef struct nodo{ 
  struct nodo *sig; 
  int info;
}Nodo;
Nodo* crear_nodo(int info){
  Nodo* temp=(Nodo*)malloc(sizeof(Nodo));
  temp->sig=NULL;
  temp->info=info;
  return temp;
} 
void recorre_lista(Nodo *N){
  while(N!=NULL){
    printf("%d\n",N->info);
    N=N->sig;
  }
}
/*init */
int main(int argv,char *agrv[]){
  Nodo *t=crear_nodo(1);
  Nodo *t2=crear_nodo(2);
  t->sig=t2;
  t2->sig=crear_nodo(4);
  recorre_lista(t);
  return 0;
}
