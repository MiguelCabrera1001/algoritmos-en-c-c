#include <stdio.h>
#include <malloc.h>
#define Tipo int
/*
 * Estrunctura basica de un arbol binario 
 * Funciones de recorrido de un arbol usando recursividad
 * Orden {Raiz,izquierda,derecha}
 * preorden {Izquierda, Raiz, Derecha}
 * postOrden {Izquierda, Derecha, Raiz}
 * @miguelCabrera 2-dic-15 :D
*/

typedef struct nodo{
  struct nodo *izq,*der;
  Tipo info; 
}Rama;

Rama* crear_rama(Tipo info){
  Rama *temp=(Rama*)malloc(sizeof(Rama));
  temp->info=info;
  temp->izq=temp->der=NULL;
  return temp;
}
void Orden(Rama *N){
 if(N==NULL){  return;}
 printf("%d, ",N->info); 
 Orden(N->izq);
  Orden(N->der);
}
void preOrden(Rama *N){
 if(N==NULL){  return;} 
  Orden(N->izq);
  printf("%d, ",N->info);
  Orden(N->der);

}
void PostOrden(Rama *N){
  if(N==NULL){  return;}
  Orden(N->izq);
  Orden(N->der);  
  printf("%d, ",N->info);
}
int main(void){
  Rama *t=crear_rama(1);
  t->izq=crear_rama(2);
  t->der=crear_rama(3);
  Orden(t);
  printf("\n");
  preOrden(t);
  printf("\n");
  PostOrden(t);
 return 0;
}
