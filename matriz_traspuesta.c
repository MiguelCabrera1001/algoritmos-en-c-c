#include <stdio.h>
#define Tam 5
/****
*  @miguelCabrera 
*  Programa que multiplica dos matrices,
*  Primero calcula la traspuesta de una matriz y despues
*  Se multiplica m1*m1' y se almacena en la matriz 3  
*****/
void Zeros(int M[][Tam]){
  int i,j;
  for(i=0;i<Tam;i++){
    for(j=0;j<Tam;j++)
      M[i][j]=0;
  }
}
void init(int M[][Tam]){
  int i,j;
  for(i=0;i<Tam;i++){
    for(j=0;j<Tam;j++)
      M[i][j]=random()%10;
  }

}
void print_matriz(int M[][Tam]){
  int i,j;
  for(i=0;i<Tam;i++){
    for(j=0;j<Tam;j++){
      printf("%d ",M[i][j]);
    }
    printf("\n");
  }

}
void matriz_transpuesta(int matriz[][Tam],int  matriz2[][Tam]){
  int i=0,j=0;
  for (i=0;i<Tam;i++)
    for(j=0;j<Tam;j++)
      matriz2[j][i]=matriz[i][j];
}
void matriz_multplica(int matriz[][Tam],int  matriz2[][Tam],int matriz_result[][Tam]){

  int i=0,j=0,k=0,r=0;
  for (i=0;i<Tam;i++){ //
    for(j=0;j<Tam;j++){ //
      for(k=0;k<Tam;k++){ //
	matriz_result[i][j]+=(matriz[j][k]*matriz2[k][i]);//(matriz[k][i]*matriz2[j][k]);
      }
    }
  }

}
int main(int arrg,char *argv[]){
  int m1[Tam][Tam],m2[Tam][Tam],m3[Tam][Tam];
  init(m1);
  Zeros(m3);
  print_matriz(m1);
  matriz_transpuesta(m1,m2);
  printf("\n");
  print_matriz(m2);
  printf("\n");
  // del estilo m3=m1*m2; 
  matriz_multplica(m1,m2,m3); 
  print_matriz(m3);
  return 0;
}
