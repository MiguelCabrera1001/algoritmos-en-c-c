#include <stdio.h>
#define MAX_SIZE 10
/**
   @miguelCabrera
   Programa que simula una pila doble utilizando  array's 
   se implementan las funciones clasicas de push, pop empty y 
   print_satck para imprimir la pila :D
**/
typedef struct stack{
  int array[MAX_SIZE];
  int top; 
}Stack;
typedef struct sdouble{
  Stack *frente,*atras;
  int poiter;
}Stack_d;

void init_Stack(Stack *p){
  int i=0;
  for(i=0;i<MAX_SIZE;i++)
    p->array[i]=0;
    p->top=-1;
}
int empty(Stack *p){
  if(p->top==-1)
    return 1;
  return 0;
}
void push(Stack *p,int info){
  p->array[++p->top]=info;
}
int pop(Stack *p){
  return p->array[p->top--];
}
void print_stack(Stack *p){
  Stack s=*p;
  while(!empty(&s)){
    printf("|%d|\n",pop(&s));
  }    
}

int main(int argc,char *argv[]){
  Stack *p;
   init_Stack(p);
   /*push(&p,1);
  
     push(&p,2);  push(&p,3);  push(&p,4);*/
  print_stack(p);
  return 0;
}
